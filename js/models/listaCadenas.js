class listaCadenas
{
    constructor(listaNueva=null)
    {
        if(listaNueva!=null)
        {
            this.lista=listaNueva;
            this.listaAnterior=[];
        }
        else
        {
            this.lista=[];
            this.listaAnterior=[];
        }
        
    }

    addCadena(cadena)
    {
        if(cadena=="") return 0;

        if(this.lista.indexOf(cadena)==-1)
        {
            this.lista.push(cadena)
        }
        else
        {
            alert("La cadena ya existe, agregue otra");
        }
    }

    removeCadena(cadena)
    {
        this.lista.splice(this.lista.indexOf(cadena),1)
    }

    toSelect()
    {
        var htmlSelect="";
        for(var x in this.lista)
        {
            htmlSelect+="<option>"+this.lista[x]+"</option>"
        }
        return htmlSelect;
    }

    reDo(){
        this.lista=this.listaAnterior;
    }
}