var lista= new listaCadenas();
var borradas=[];
var agregadas=[];

$('#btnAgregar').click(function(){
    borradas=[];
    agregadas=[];
    nueva_cadena=prompt("Ingrse la nueva cadena:")
    agregadas.push(nueva_cadena);
    lista.addCadena(nueva_cadena)
    actualizarCadenas();
})

$('#btnQuitar').click(function(){
     borradas=[];
     agregadas=[];
    for(var i in $('#selectCadenas').val())
    {
        borradas.push($('#selectCadenas').val()[i]);
        lista.removeCadena($('#selectCadenas').val()[i])
    }


    actualizarCadenas();
})


function actualizarCadenas()
{
    $('#selectCadenas').html(lista.toSelect());
}


//se cargan datos de cadenas predefindas
$.mockjax({
    url: "hello.php",
    proxy: './js/sample.json',
    responseTime: 0,
    dataType: 'json'
});

$.ajax({
    url: "hello.php",
    dataType: 'json'
 
}).done(function (json_response) {
      borradas=[];
     agregadas=[];
    for (i in json_response.employees) {
        agregadas.push(json_response.employees[i].firstName);
        lista.addCadena(json_response.employees[i].firstName);
    }
    
    actualizarCadenas();    
});

// functionalidad de regreso
function reDo()
{
    for(var x in borradas)
    {
        lista.addCadena(borradas[x])
    }
    for(var i in agregadas)
    {
        lista.removeCadena(agregadas[i])
    }
     borradas=[];
     agregadas=[];  
    actualizarCadenas();
}


$('#btnReDo').click(function(){
        reDo();
})